#include "sort.hpp"
#include <cstddef>
#include <iterator>
#include <utility>
#include <cmath>
#include <algorithm>

namespace sort
{
    namespace
    {
        info algo_stat;

        struct list
        {
            list() : value(0), next(nullptr) {}
            list(int _value, list *_list) : value(_value), next(_list) {}

            int value;
            list *next;
        };

        auto gt(const int lhs, const int rhs)
        {
            algo_stat.comparison++;
            return lhs > rhs;
        }
        auto gte(const int lhs, const int rhs)
        {
            algo_stat.comparison++;
            return lhs >= rhs;
        }
        auto lt(const int lhs, const int rhs)
        {
            algo_stat.comparison++;
            return lhs < rhs;
        }
        auto lte(const int lhs, const int rhs)
        {
            algo_stat.comparison++;
            return lhs <= rhs;
        }

        iter_t max(iter_t begin, iter_t end)
        {
            if (begin != end)
            {
                auto max = begin;
                while (++begin != end)
                {
                    if (gt(*begin, *max))
                        max = begin;
                }
                return max;
            }
            else
                return end;
        }
    }

    info bucket(std::vector<int> &vec)
    {
        algo_stat.name = "bucket";
        algo_stat.elements = vec.size();
        algo_stat.comparison = 0;
        algo_stat.assignment = 0;

        auto max_value = *max(vec.begin(), vec.end()) + 1;
        std::vector<list*> buckets(vec.size(), nullptr);
        const auto coef = vec.size() / (long double) max_value;

        for (auto it = vec.begin(); it < vec.end(); ++it)
        {
            auto b = (long long) (*it * coef); algo_stat.assignment++;
            buckets.at(b) = new list{*it, buckets.at(b)}; algo_stat.assignment++;

            auto item = buckets.at(b); algo_stat.assignment++;
            while (item->next != nullptr)
            {
                if (lte(item->value, item->next->value))
                    break;

                std::swap(item->value, item->next->value); algo_stat.assignment += 3;
                item = item->next; algo_stat.assignment++;
            }

        }

        auto src = buckets.begin();
        auto dest = vec.begin();
        for (; src < buckets.end() && dest < vec.end(); ++src)
        {
            if (*src == nullptr)
                continue;

            auto node = *src; algo_stat.assignment++;
            while (node != nullptr)
            {
                *dest = node->value; algo_stat.assignment++;
                auto tmp = node; algo_stat.assignment++;
                node = node->next; algo_stat.assignment++;
                std::free(tmp);
                ++dest;
            }
        }

        return algo_stat;
    }

    info counting(std::vector<int> &vec)
    {
        algo_stat.name = "counting";
        algo_stat.elements = vec.size();
        algo_stat.comparison = 0;
        algo_stat.assignment = 0;

        std::vector<int> counters(*max(vec.begin(), vec.end()) + 1, 0);
        for (std::size_t i = 0; i < vec.size(); ++i)
        {
            ++counters.at(vec.at(i));
        }

        for (std::size_t i = 1; i < counters.size(); ++i)
        {
            counters.at(i) += counters.at(i-1);
        }

        std::vector<int> result(vec.size());
        for (std::size_t i = vec.size() - 1; i < vec.size(); --i)
        {
            result.at(--counters.at(vec.at(i))) = std::move(vec.at(i));
            algo_stat.assignment++;
        }

        vec.swap(result);
        algo_stat.assignment += 3 * result.size();

        return algo_stat;
    }

    info radix(std::vector<int> &vec)
    {
        algo_stat.name = "radix";
        algo_stat.elements = vec.size();
        algo_stat.comparison = 0;
        algo_stat.assignment = 0;

        auto bits_to_compare = 0U;
        if (vec.size() < 1000)
            bits_to_compare = 4;
        else if (vec.size() < 100'000)
            bits_to_compare = 8;
        else if (vec.size() < 10'000'000'000)
            bits_to_compare = 16;
        //else call sort::counting
        std::vector<int> counters(static_cast<unsigned>(std::pow(2, bits_to_compare)));


        auto max_value = *max(vec.begin(), vec.end());
        auto max_bits = static_cast<unsigned>(std::ceil(std::log2(max_value)));

        std::vector<int> result(vec.size());
        for (auto shift = 0U; shift < max_bits; shift += bits_to_compare)
        {
            unsigned mask = ((1 << bits_to_compare) - 1) << shift;

            for (std::size_t i = 0; i < vec.size(); ++i)
                ++counters.at((vec.at(i) & mask) >> shift);

            for (std::size_t i = 1; i < counters.size(); ++i)
                counters.at(i) += counters.at(i-1);

            for (std::size_t i = vec.size() - 1; i < vec.size(); --i)
                result.at(--counters.at((vec.at(i) & mask) >> shift)) = std::move(vec.at(i));

            vec.swap(result); algo_stat.assignment += 3 * result.size();
            std::fill(counters.begin(), counters.end(), 0);
        }

        return algo_stat;
    }
}
